module wrong.wang/go-isso

go 1.13

require (
	github.com/gorilla/mux v1.7.3
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337 // indirect
	gopkg.in/ini.v1 v1.49.0
)
