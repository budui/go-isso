// Package isso provides main struct and version.
package isso

var (
	// BuildTime is the build time of go-isso.
	BuildTime string
	// Version of the go-isso
	Version string
)
